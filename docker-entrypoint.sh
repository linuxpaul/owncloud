#!/bin/bash
set -e

export IPADDR=`hostname -I`
export OC_PASS=$OWNCLOUD_ADMIN_PW

if [ -z "$FQDN" ]; then
	echo >&2 '  You may specify FQDN !!!! '
	exit 1
fi

if [ ! -e '/srv/http/owncloud/data' ]; then
	mkdir -p /srv/http/owncloud/data
	chown www-data:www-data /srv/http/owncloud/data
fi

if [ ! -e '/etc/nginx/ssl' ]; then
	mkdir /etc/nginx/ssl
	openssl genrsa -out /etc/nginx/ssl/owncloud.key 4096
	openssl req -new -sha256 -key /etc/nginx/ssl/owncloud.key -out /etc/nginx/ssl/owncloud.csr -subj "/C=DE/ST=OC/L=OC/O=Global Security/OU=IT Department/CN=$FQDN"
	openssl x509 -req -sha256 -days 3650 -in /etc/nginx/ssl/owncloud.csr -signkey /etc/nginx/ssl/owncloud.key -out /etc/nginx/ssl/owncloud.crt
fi

if [ ! -f "/var/www/owncloud/config/configuration.done" ]; then
    echo "Creating configuration in /var/www/owncloud/config"
    sed -i "s/server_name localhost/server_name $FQDN/g" /etc/nginx/sites-available/owncloud
    cd /var/www/owncloud/
    until sudo -u www-data php occ maintenance:install --database "mysql" --database-host $OWNCLOUD_DB_HOST --database-name $OWNCLOUD_DB_NAME --database-user $OWNCLOUD_DB_USER --database-pass $OWNCLOUD_DB_PASSWORD --admin-user $HOSTNAME --admin-pass $OWNCLOUD_ADMIN_PW
    do
      sleep 1
    done
    sudo -u www-data php occ config:system:set trusted_domains 1 --value=`echo $IPADDR`
    sudo -u www-data php occ config:system:set trusted_domains 2 --value=`echo $FQDN`
    if [ ! -e '/var/www/owncloud/data/setadmin.done' ]; then
      su -s /bin/sh www-data -c 'php occ user:add --password-from-env --display-name=$OWNCLOUD_ADMIN --group="admin" $OWNCLOUD_ADMIN'
      touch /var/www/owncloud/data/setadmin.done
    fi
    sudo -u www-data php occ user:delete $HOSTNAME
    touch /var/www/owncloud/config/configuration.done
fi
export OC_PASS=

/etc/init.d/php7.0-fpm start

exec "$@"